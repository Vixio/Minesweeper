package com.lv;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by christian on 02/11/15.
 */
public class Minesweeper {

    enum Difficulty {
        LOW,
        MEDIUM,
        HIGH;

        public static Difficulty fromInteger(int x) {
            switch(x) {
                case 1:
                    return LOW;
                case 2:
                    return MEDIUM;
                case 3:
                    return HIGH;
            }

            return null;
        }
    }

    static class Cell {
        enum ContentType {
            BOMB,
            DIGIT,
            EMPTY
        }

        enum State {
            COVERED,
            UNCOVERED,
            MARKED
        }

        private ContentType _contentType;
        private State _state;
        private int _digit;

        public Cell() {
            _state = State.COVERED;
            _contentType = ContentType.EMPTY;
        }

        ContentType uncover() {
            _state = State.UNCOVERED;
            return _contentType;
        }

        public void toggleMark() {
            if ( _state == State.COVERED ) {
                _state = State.MARKED;
            } else if ( _state == State.MARKED ) {
                _state = State.COVERED;
            }
        }

        public void setAsEmpty() {
            _contentType = ContentType.EMPTY;
            _digit = 0;
        }

        public void setAsDigit(int digit) {
            _contentType = ContentType.DIGIT;
            _digit = digit;
        }

        public void setAsBomb() {
            _contentType = ContentType.BOMB;
            _digit = 0;
        }

        public int getDigit() {
            return _digit;
        }

        public ContentType getContentType() {
            return _contentType;
        }

        public State getState() {
            return _state;
        }

        @Override
        public String toString() {
            if ( _state == State.COVERED ) {
                return "-";
            }

            if ( _state == State.MARKED ) {
                return "M";
            }

            if ( _contentType == ContentType.BOMB ) {
                return "*";
            } else if ( _contentType == ContentType.DIGIT ) {
                return String.valueOf(_digit);
            }

            return " ";
        }
    }

    private Cell[][] _board;
    private boolean _isGameOver;
    private int _countNumOfUncoveredCells;
    private int _totalEmptyCells;

    public Minesweeper(Difficulty difficulty) {
        resetGame(difficulty);
    }

    public void resetGame(Difficulty difficulty) {
        _countNumOfUncoveredCells = 0;
        _isGameOver = false;

        if ( difficulty == Difficulty.LOW ) {
            _board = new Cell[9][9];
        } else if ( difficulty == Difficulty.MEDIUM ) {
            _board = new Cell[16][16];
        } else {
            _board = new Cell[16][30];
        }

        for ( int r = 0; r < _board.length; r++) {
            for ( int c = 0; c < _board.length; c++ ) {
                _board[r][c] = new Cell();
            }
        }

        if ( difficulty == Difficulty.LOW ) {
            populateWithBombs(0.1f);
        }
        else if ( difficulty == Difficulty.MEDIUM ) {
            populateWithBombs(0.15f);
        }
        else {
            populateWithBombs(0.2f);
        }

        updateBoardDigits();
    }

    public Cell uncover(int row, int column) {
        return uncover(row, column, false);
    }

    private Cell uncover(int row, int column, boolean uncoverMarked) {
        if ( isGameOver() ) {
            return null;
        }

        if ( _board[row][column].getState() == Cell.State.UNCOVERED ) {
            return null;
        }

        if ( uncoverMarked == false && _board[row][column].getState() == Cell.State.MARKED ) {
            return null;
        }

        Cell.ContentType contentType = _board[row][column].uncover();

        _countNumOfUncoveredCells++;
        if ( _countNumOfUncoveredCells == _totalEmptyCells ) {
            _isGameOver = true;
        }

        if ( contentType == Cell.ContentType.BOMB ) {
            _isGameOver = true;
            return _board[row][column];
        }

        if ( contentType == Cell.ContentType.DIGIT ) {
            return _board[row][column];
        }

        int dr[] = { -1, -1, -1,  0, 0,  1, 1, 1 };
        int dc[] = { -1,  0,  1, -1, 1, -1, 0, 1 };

        for ( int i = 0; i < dr.length; i++ ) {
            try { uncover(row + dr[i], column + dc[i], true);  } catch (IndexOutOfBoundsException e) {}
        }

        return _board[row][column];
    }

    public void toggleMark(int row, int col) {
        _board[row][col].toggleMark();
    }

    public boolean isGameOver() {
        return _isGameOver;
    }

    private void populateWithBombs(float ratioBomb) {
        Random rand = new Random();

        int totalBombs = (int)(_board.length * _board[0].length * ratioBomb);
        int numPlacedBombs = 0;

        _totalEmptyCells = ( _board.length * _board[0].length ) - totalBombs;

        while ( numPlacedBombs < totalBombs ) {
            int row    = rand.nextInt(_board.length - 1);
            int column = rand.nextInt(_board[0].length - 1);

            if ( _board[row][column].getContentType() != Cell.ContentType.BOMB ) {
                _board[row][column].setAsBomb();
                numPlacedBombs++;
            }
        }
    }

    private void updateBoardDigits() {
        for ( int r = 0; r < _board.length; r++ ) {
            for ( int c = 0; c < _board[r].length; c++ ) {

                if ( _board[r][c].getContentType() == Cell.ContentType.BOMB ) {
                    continue;
                }

                int digit = 0;

                int dr[] = { -1, -1, -1,  0, 0,  1, 1, 1 };
                int dc[] = { -1,  0,  1, -1, 1, -1, 0, 1 };

                for ( int i = 0; i < dr.length; i++ ) {
                    try {
                        if ( _board[r + dr[i]][c + dc[i]].getContentType() == Cell.ContentType.BOMB ) {
                            digit++;
                        }
                    } catch (IndexOutOfBoundsException e) {}
                }

                if ( digit == 0 ) {
                    _board[r][c].setAsEmpty();
                } else {
                    _board[r][c].setAsDigit(digit);
                }
            }
        }
    }

    public int getBoardHeight() {
        return _board.length;
    }

    public int getBoardWidth() {
        return _board[0].length;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        for ( int r = 0; r < _board.length; r++ ) {
            stringBuilder.append(r);
            stringBuilder.append(") ");
            stringBuilder.append(Arrays.toString(_board[r]));
            stringBuilder.append('\n');
        }

        return stringBuilder.toString();
    }
}
