package com.lv;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by christian on 02/11/15.
 */
public class MinesweeperGame {
    private Scanner scanner = new Scanner(System.in);
    private Minesweeper minesweeper;

    public void run() {
        boolean playAgain = true;

        while ( playAgain ) {
            Minesweeper.Difficulty difficulty =  askDifficulty();
            minesweeper = new Minesweeper(difficulty);

            while (!minesweeper.isGameOver()) {
                System.out.println(minesweeper);


                System.out.println("1) Destapar");
                System.out.println("2) Alternar marca");

                int option = nextInt("", 1, 2, "Ingrese una opción valida");

                int row, column;
                if ( option == 1 ) {
                    System.out.println("Destapar celda");
                } else {
                    System.out.println("Alternar marca en celda");
                }

                row    = nextInt("Fila: ", 0, minesweeper.getBoardHeight(), "Ingrese una fila valida");
                column = nextInt("Columnna: ", 0, minesweeper.getBoardHeight(), "Ingrese una fila valida");

                if ( option == 1 ) {
                    minesweeper.uncover(row, column);
                } else {
                    minesweeper.toggleMark(row, column);
                }
            }

            System.out.println(minesweeper);

            int n = nextInt("El juego ha terminado. Si deseas jugar de nuevo ingresa un 1: ", "");
            playAgain = ( n == 1 );
        }
    }

    private Minesweeper.Difficulty askDifficulty() {
        System.out.println("Nivel de dificultad:");
        System.out.println("1) Fácil");
        System.out.println("2) Normal");
        System.out.println("3) Dificil");

        System.out.println("Opcion: ");

        int option = nextInt("", 1, 3, "Ingrese una opción del 1 al 3");
        return Minesweeper.Difficulty.fromInteger(option);
    }


    private int nextInt(String message, String errorMessage) {
        int value = 0;
        do {
            if ( message != null && ! message.isEmpty()) {
                System.out.print(message);
            }

            try {
                value = scanner.nextInt();
            } catch (InputMismatchException e) {
                scanner.nextLine();
                System.out.println("Debe ingresar una cantidad numerica");
                continue;
            }

            //System.out.println(errorMessage);
        } while ( false );

        return value;
    }

    private int nextInt(String message, int minValue, int maxValue, String errorMessage) {
        int value;
        do {
            if ( message != null && ! message.isEmpty()) {
                System.out.print(message);
            }

            try {
                value = scanner.nextInt();
            } catch (InputMismatchException e) {
                scanner.nextLine();
                System.out.println("Debe ingresar una cantidad numerica en un rango del " + minValue + " al " + maxValue);
                continue;
            }

            if ( value >= minValue && value <= maxValue ) {
                break;
            }

            System.out.println(errorMessage);
        } while ( true );

        return value;
    }
}
